using UnityEngine;

namespace AttaboysGames.KeyValueAttribute.Package.Runtime
{
	public class KeyValueAttribute : PropertyAttribute
	{
		public readonly string PropertyName;

		public KeyValueAttribute(string propertyName)
		{
			PropertyName = propertyName;
		}
	}
}