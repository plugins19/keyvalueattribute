using System;
using System.Reflection;
using UnityEditor;

namespace AttaboysGames.KeyValueAttribute.Package.Editor.Utils
{
	public static class EditorHelper
	{
		public static object GetGenericValue(this SerializedProperty property)
		{
			var targetObject = (object)property.serializedObject.targetObject;
			var strings = property.propertyPath.Split('.');
			
			for (var i = 0; i < strings.Length; i++)
			{
				var path = strings[i];
				
				if (path == "Array")
				{
					var data = strings[++i];
					var indexOfStart = data.IndexOf('[');
					data = data.Remove(0, indexOfStart + 1);
					var dataIndex = int.Parse(data.Remove(data.Length - 1, 1));
					targetObject = ((Array)targetObject).GetValue(dataIndex);
					continue;
				}

				var type = targetObject.GetType();
				var field = type.GetField(path,
					BindingFlags.Default | BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
				
				if (field == null)
				{
					targetObject = null;
					break;
				}

				targetObject = field.GetValue(targetObject);
			}

			return targetObject?.ToString() ?? "Null";
		}
	}
}