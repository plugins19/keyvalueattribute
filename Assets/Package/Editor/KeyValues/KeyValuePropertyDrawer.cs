using System;
using System.Globalization;
using AttaboysGames.KeyValueAttribute.Package.Editor.Utils;
using UnityEditor;
using UnityEngine;

namespace AttaboysGames.KeyValueAttribute.Package.Editor.KeyValues
{
	[CustomPropertyDrawer(typeof(Runtime.KeyValueAttribute))]
	public class KeyValuePropertyDrawer : PropertyDrawer
	{
		public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
			=> EditorGUI.GetPropertyHeight(property, label, true);

		protected virtual Runtime.KeyValueAttribute Attribute => (Runtime.KeyValueAttribute)attribute;

		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			var fullPathName = property.propertyPath + "." + Attribute.PropertyName;
			var titleNameProp = property.serializedObject.FindProperty(fullPathName);
			var newLabel = GetTitle(titleNameProp);

			if (string.IsNullOrEmpty(newLabel))
				newLabel = label.text;

			EditorGUI.PropertyField(position, property, new GUIContent(newLabel, label.tooltip), true);
		}

		private static string GetTitle(SerializedProperty titleNameProp)
		{
			if (titleNameProp == null)
				return "KeyValue -> Null";

			try
			{
				return titleNameProp.propertyType switch
				{
					SerializedPropertyType.Integer => titleNameProp.intValue.ToString(),
					SerializedPropertyType.Boolean => titleNameProp.boolValue.ToString(),
					SerializedPropertyType.Float => titleNameProp.floatValue.ToString(CultureInfo.InvariantCulture),
					SerializedPropertyType.String => titleNameProp.stringValue,
					SerializedPropertyType.Color => titleNameProp.colorValue.ToString(),
					SerializedPropertyType.ObjectReference => titleNameProp.objectReferenceValue.ToString(),
					SerializedPropertyType.Enum => titleNameProp.enumNames[titleNameProp.enumValueIndex],
					SerializedPropertyType.Vector2 => titleNameProp.vector2Value.ToString(),
					SerializedPropertyType.Vector3 => titleNameProp.vector3Value.ToString(),
					SerializedPropertyType.Vector4 => titleNameProp.vector4Value.ToString(),
					SerializedPropertyType.Generic => titleNameProp.GetGenericValue()?.ToString() ?? "Null",
					_ => $"{titleNameProp.propertyType} Key not Implemented"
				};
			}
			catch (Exception e)
			{
				Debug.LogError($"[{nameof(KeyValuePropertyDrawer)}] {e}");
				return "KeyValue -> Exception";
			}
		}
	}
}